let btn_add = document.querySelector(".send-nota")
let btn_media = document.querySelector(".calcula-media")
let btn_restaurar = document.querySelector(".restaurar")
let notas = document.querySelector(".notas")
var index = 1

btn_media.disabled = true

function add_nota() {
    let input = document.querySelector(".input")
    if(isNaN(input.value) || input.value > 10 || input.value < 0){
        alert("A nota digitada é inválida, por favor tente novamente")
    }else if(input.value == ""){
        alert("Por favor, insira uma nota")
    }else{
        let nota = document.createElement("p")
        nota.classList.add("nota")
        nota.innerText = "A nota " + (index++) + " foi " + input.value
        notas.append(nota)
    }
    input.value = ""
    btn_media.disabled = false
}

function calcula_media() {
    let nota = document.querySelectorAll(".nota")
    let text = document.querySelector(".media")
    let media = 0
    for(let i = 0; i < nota.length; i++){
        var string = nota[i].innerText.split(" ")
        media += parseFloat(string[string.length - 1])
    }
    media /= nota.length
    text.innerText += " " + media.toFixed(1)
    btn_add.disabled = true
    btn_media.disabled = true
}

function restaura_sistema() {
    btn_media.disabled = true
    btn_add.disabled = false
    let box = document.querySelector(".notas")
    box.innerHTML = ""
    let text = document.querySelector(".media")
    text.innerText = text.innerText.substr(0,10)
    index = 1
}

btn_add.addEventListener("click", add_nota)
btn_media.addEventListener("click", calcula_media)
btn_restaurar.addEventListener("click", restaura_sistema)